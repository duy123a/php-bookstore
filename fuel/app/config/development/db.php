<?php
/**
 * Fuel is a fast, lightweight, community driven PHP 5.4+ framework.
 *
 * @package    Fuel
 * @version    1.8.2
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2019 Fuel Development Team
 * @link       https://fuelphp.com
 */

/**
 * -----------------------------------------------------------------------------
 *  Database settings for development environment
 * -----------------------------------------------------------------------------
 *
 *  These settings get merged with the global settings.
 *
 */

return array(
	'default' => array(
		'connection' => array(
			'dsn'      => 'mysql:host=localhost;dbname=book_db',
			'username' => 'root',
			'password' => '',
		),
	),
);

# 000webhost database

// return array(
// 	'default' => array(
// 		'connection' => array(
// 			'dsn'      => 'mysql:host=localhost;dbname=id16747768_book_db',
// 			'username' => 'id16747768_root',
// 			'password' => 'H6y^}lbnw#tDoys@',
// 		),
// 	),
// );